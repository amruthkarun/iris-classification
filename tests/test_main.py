import pytest
import json

""" Main test file """


def test_ping(test_app):
    response = test_app.get("/")
    assert response.status_code == 200
    assert response.json() == {"Welcome": "Iris Classification"}


def test_predict_class1(test_app):
    inp_file = open('inputs/predict_class1.json', 'r')
    response = test_app.post(
        "/predict",
        json=json.load(inp_file),
    )
    assert response.status_code == 200
    assert response.json()['class'] == 'Iris Setosa'


def test_predict_class2(test_app):
    inp_file = open('inputs/predict_class2.json', 'r')
    response = test_app.post(
        "/predict",
        json=json.load(inp_file),
    )
    assert response.status_code == 200
    assert response.json()['class'] == 'Iris Versicolor'


def test_predict_class3(test_app):
    inp_file = open('inputs/predict_class3.json', 'r')
    response = test_app.post(
        "/predict",
        json=json.load(inp_file),
    )
    assert response.status_code == 200
    assert response.json()['class'] == 'Iris Virginica'


def test_input_fail(test_app):
    with pytest.raises(KeyError):
        response = test_app.post(
            "/predict",
            json={}
        )
