FROM python:3.7

WORKDIR /iris

copy requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY models models

COPY tests tests

COPY app app

CMD uvicorn app.main:app --host "0.0.0.0" --port $PORT
