import numpy as np
import joblib


class IrisClassifier:
    """
    Classifies different species of Iris flower based on
    sepal length, sepal width, petal length and petal width
    """

    def __init__(self):
        self.model = joblib.load('models/iris_classifier.joblib')
        self.classes = {
            0: 'Iris Setosa',
            1: 'Iris Versicolor',
            2: 'Iris Virginica'
        }

    def predict(self, features: dict):
        """
        Predicts to the species of Iris based on
        input features using Iris classification model
        Arguments:
        features  -- input features
        Returns: class and probability value for the prediction
        """

        prediction = {}
        try:
            input_data = [features['sepal_length'], features['sepal_width'],
                          features['petal_length'], features['petal_width']]
            prediction = self.model.predict_proba([input_data])
        except KeyError as e:
            print(e)

        return {'class': self.classes[np.argmax(prediction)],
                'probability': round(max(prediction[0]), 2)}
