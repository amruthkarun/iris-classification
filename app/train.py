from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import tree
from joblib import dump


def train_model():
    """
    Trains the Iris dataset and creates
    a classification model
    Arguments: None
    Returns: train and test accuracy score
    """

    iris = datasets.load_iris()
    x = iris.data
    y = iris.target

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
    classifier = tree.DecisionTreeClassifier()
    classifier.fit(x_train, y_train)

    y_train_predicted = classifier.predict(x_train)
    train_accuracy = 100 * accuracy_score(y_train, y_train_predicted)
    y_test_predicted = classifier.predict(x_test)
    test_accuracy = 100 * accuracy_score(y_test, y_test_predicted)

    # Saving the Pipeline
    dump(classifier, 'models/iris_classifier.joblib')
    return {'train_accuracy': train_accuracy,
            'test_accuracy': test_accuracy}


