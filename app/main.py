from fastapi import FastAPI
import app.train as train
from app.iris_classifier import IrisClassifier
import app.storage as st

app = FastAPI()


@app.post('/predict')
async def predict_species(features: dict):
    """
    Predicts to the species of Iris based on
    input features
    Arguments:
    features  -- input features
    Returns:
    classification_results  --  final classification results
    """

    classifier = IrisClassifier()
    classification_results = classifier.predict(features)
    st.save_results(features, classification_results)
    return classification_results


@app.post('/train')
async def train_dataset():
    """
    Trains the Iris dataset to create a classification model
    Arguments: None
    Returns:
    accuracy_score  -- train and test accuracy score
    """

    accuracy_score = train.train_model()
    print(accuracy_score)
    return accuracy_score


@app.get("/")
async def read_main():
    """ Application root """
    return {"Welcome": "Iris Classification"}
