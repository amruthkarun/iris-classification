import os
import psycopg2

DATABASE_URL = os.getenv('DATABASE_URL')


def create_connection():
    """
    Establishes a connection with PostgresDB
    Arguments: None
    Returns:
    connection  --  database connection object
    """

    # Connect to an existing database
    connection = psycopg2.connect(DATABASE_URL)

    return connection


def create_table(cursor):
    """
    Creates a table to store classification results
    Arguments:
    cursor  -- cursor for performing database operations
    Returns: None
    """

    # SQL query to create a new table
    create_table_query = '''CREATE TABLE IF NOT EXISTS IRIS
              (SEPAL_LENGTH FLOAT, SEPAL_WIDTH FLOAT,
              PETAL_LENGTH FLOAT, PETAL_WIDTH FLOAT,
              CLASS VARCHAR(20), PROBABILITY FLOAT);'''

    cursor.execute(create_table_query)


def save_results(features, result):
    """
    Saves classification results to a PostgresDB
    Arguments:
    features  -- input features
    result    -- classification result
    Returns: None
    """
    if (DATABASE_URL != None):
        connection = create_connection()
        cursor = connection.cursor()
        create_table(cursor)

        insert_query = """INSERT INTO IRIS VALUES (%s, %s, %s, %s, %s, %s)"""
        item_tuple = (features['sepal_length'], features['sepal_width'],
                      features['petal_length'], features['petal_width'],
                      result['class'], result['probability'])
        cursor.execute(insert_query, item_tuple)
        connection.commit()
        print("1 Record inserted successfully!")
    else:
        print("Database not configured!")
